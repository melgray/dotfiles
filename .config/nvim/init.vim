call plug#begin()
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'preservim/nerdtree'
"Plug 'vim-airline/vim-airline'
"Plug 'vim-airline/vim-airline-themes'
Plug 'dikiaap/minimalist'
Plug 'rust-lang/rust.vim'
"Plug 'tikhomirov/vim-glsl'
"Plug 'blindFS/vim-taskwarrior'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'dylanaraps/wal.vim'
"Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}
"Plug 'dhruvasagar/vim-table-mode'
"Plug 'preservim/nerdcommenter'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'mhinz/vim-startify'
"Plug 'kdheepak/lazygit.nvim', { 'branch': 'nvim-v0.4.3' }
"Plug 'reasonml-editor/vim-reason-plus'
"Plug 'tomlion/vim-solidity'
"Plug 'numirias/semshi', {'do': ':UpdateRemotePlugins'}
"Plug 'Vimjas/vim-python-pep8-indent'
"Plug 'leafgarland/typescript-vim'
"Plug 'maxmellon/vim-jsx-pretty'
"Plug 'ryanoasis/vim-devicons'
"Plug 'glench/vim-jina2-syntax'
"Plug 'kyazdani42/nvim-web-devicons', {'branch': 'neovim-0.4'}
"Plug 'romgrk/barbar.nvim'
call plug#end()

call coc#add_extension(
  \ 'coc-rust-analyzer',
\ )

set t_Co=256
syntax on
"colorscheme minimalist
colorscheme wal

" Configure airline
"let g:airline#extensions#tabline#enabled = 1
"let g:airline_theme='minimalist'
"let g:airline_powerline_fonts = 1
"let g:airline#extensions#tabline#enabled = 1

" When entering a directory launch NERDTree
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | exe 'cd '.argv()[0] | endif

" Toggles NERDTree
nmap <C-b> :NERDTreeToggle<CR>

" Show hidden files
let NERDTreeShowHidden=1

" Call rustfmt whenever a file is saved
let g:rustfmt_autosave = 1
let g:rust_cargo_use_clippy = 1

" Ensure we show line numbers
set number

" Associate file types for glsl 
" autocmd! BufNewFile,BufRead *.vs,*.fs,*.glsl,*.frag,*.vert,*.compute set ft=glsl

" Associate file types with clang-format
autocmd FileType c,cpp,glsl setlocal equalprg=clang-format
autocmd BufRead,BufNewFile *.htm,*.html,*.ts,*.tsx,*.js,*.jsx,*.css,*.frag,*.vert,*.compute,*.vs,*.fs setlocal tabstop=2 shiftwidth=2 softtabstop=2

" Prettier
command! -nargs=0 Prettier :CocCommand prettier.formatFile

" This is necessary for NERDCommenter 
filetype plugin on

" vim-startify
let g:startify_session_dir = '~/.config/nvim/session'
let g:startify_lists = [
          \ { 'type': 'files',     'header': ['   Files']            },
          \ { 'type': 'dir',       'header': ['   Current Directory '. getcwd()] },
          \ { 'type': 'sessions',  'header': ['   Sessions']       },
          \ { 'type': 'bookmarks', 'header': ['   Bookmarks']      },
          \ ]
let g:startify_custom_header = [
  \'                  __gggrgM**M#mggg__',
  \'             __wgNN@"B*P""mp""@d#"@N#Nw__',
  \'             _g#@0F_a*F#  _*F9m_ ,F9*__9NG#g_',
  \'          _mN#F  aM"    #p"    !q@    9NL "9#Qu_',
  \'         g#MF _pP"L  _g@"9L_  _g""#__  g"9w_ 0N#p',
  \'       _0F jL*"   7_wF     #_gF     9gjF   "bJ  9h_',
  \'      j#  gAF    _@NL     _g@#_      J@u_    2#_  #_',
  \'     ,FF_#" 9_ _#"  "b_  g@   "hg  _#"  !q_ jF "*_09_',
  \'     F N"    #p"      Ng@       `#g"      "w@    "# t',
  \'    j p#    g"9_     g@"9_      gP"#_     gF"q    Pb L',
  \'    0J  k _@   9g_ j#"   "b_  j#"   "b_ _d"   q_ g  ##',
  \'    #F  `NF     "#g"       "Md"       5N#      9W"  j#',
  \'    #k  jFb_    g@"q_     _*"9m_     _*"R_    _#Np  J#',
  \'    tApjF  9g  J"   9M_ _m"    9%_ _*"   "#  gF  9_jNF',
  \'     k`N    "q#       9g@        #gF       ##"    #"j',
  \'     `_0q_   #"q_    _&"9p_    _g"`L_    _*"#   jAF,',
  \'      9# "b_j   "b_ g"    *g _gF    9_ g#"  "L_*"qNF',
  \'       "b_ "#_    "NL      _B#      _I@     j#" _#"',
  \'         NM_0"*g_ j""9u_  gP  q_  _w@ ]_ _g*"F_g@',
  \'          "NNh_ !w#_   9#g"    "m*"   _#*" _dN@"',
  \'             9##g_0@q__ #"4_  j*"k __*NF_g#@P"',
  \'               "9NN#gIPNL_ "b@" _2M"Lg#N@F"',
  \'                   ""P@*NN#gEZgNN@#@P""']

" lazygit
nnoremap <C-l-g> :LazyGit<CR>

" CTRL-p atom/sublime text style mapping with fzf
nnoremap <C-p> :FZF<CR>

" run clippy
let mapleader = ","
nnoremap <leader>c :!cargo clippy<CR>

" cycle through buffers with <ALT><Left> and <ALT><Right>
:nmap <M-Left> :bprev<CR>
:nmap <M-Right> :bnext<CR>

:map <M-1> :confirm :b4 <CR>
:map <M-2> :confirm :b5 <CR>
:map <M-3> :confirm :b6 <CR>
:map <M-4> :confirm :b7 <CR>
:map <M-5> :confirm :b8 <CR>
:map <M-6> :confirm :b9 <CR>

" vim-table-mode
function! s:isAtStartOfLine(mapping)
  let text_before_cursor = getline('.')[0 : col('.')-1]
  let mapping_pattern = '\V' . escape(a:mapping, '\')
  let comment_pattern = '\V' . escape(substitute(&l:commentstring, '%s.*$', '', ''), '\')
  return (text_before_cursor =~? '^' . ('\v(' . comment_pattern . '\v)?') . '\s*\v' . mapping_pattern . '\v$')
endfunction

inoreabbrev <expr> <bar><bar>
          \ <SID>isAtStartOfLine('\|\|') ?
          \ '<c-o>:TableModeEnable<cr><bar><space><bar><left><left>' : '<bar><bar>'
inoreabbrev <expr> __
          \ <SID>isAtStartOfLine('__') ?
          \ '<c-o>:silent! TableModeDisable<cr>' : '__'

" Import coc config
source $HOME/.config/nvim/coc.vim


nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

command! -nargs=0 Format :call CocAction('format')


let g:pymode_indent = 0


" merlin - ocaml
"let g:opamshare = substitute(system('opam config var share'),'\n$','','''')
"execute "set rtp+=" . g:opamshare . "/merlin/vim"
