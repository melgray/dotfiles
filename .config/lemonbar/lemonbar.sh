#!/usr/bin/env bash

# Font Awesome 5 Icons for Battery states
#batteryicon="\uf242"
battery_full_icon="\uf240"
battery_3qfull_icon="\uf241"
battery_halffull_icon="\uf242"
battery_halffull_icon="\uf242"
battery_qfull_icon="\uf243"
battery_empty_icon="\uf244"

# Font Awesome 5 Icons for speaker states
speaker_up_icon="\uf028"
speaker_down_icon="\uf027"
speaker_low_icon="\uf026"
speaker_mute_icon="\uf6a9"

wifiicon="📻"

wifi_ifname="iwx0"

padding="  "
wBackground="#3f3f3f"
wForeground="#000000"
wForegroundCurrent="#ffffff"

# Define the workspaces
Workspaces() {
    all=$(bspc query -D)
    occupied=$(bspc query -D -d .occupied)
    current=$(bspc query -D -d .focused)

    result=""
    while IFS= read -r line; do
        name=$(bspc query -d "$line" -D --names)

        if [[ "$current" == *"$line"* ]];
        then
          result="$result%{B$wBackground}%{F$wForegroundCurrent}"
          result="$result$padding$name$padding"
          result="$result%{F-}%{B-}"
        else
            if [[ "$occupied" == *"$line"* ]];
            then
                result="$result%{B$wBackground}%{F$wForeground}"
                result="$result$padding$name$padding"
                result="$result%{F-}%{B-}"
            fi
        fi

    done <<< "$all"
    echo -e "$result"
}

Audio() {
    OUTPUT_VOLUME=$(sndioctl output.level | cut -d '=' -f 2 | xargs echo "100 * " | bc | cut -d '.' -f 1)
    OUTPUT_MUTED=$(sndioctl output.mute | cut -d '=' -f 2)
    if [[ $OUTPUT_MUTED == 1 ]];
    then
        printf "%s %s" "$speaker_mute_icon" "0%"
    else
        speaker_icon=$speaker_up_icon
        if (( OUTPUT_VOLUME <= 75 && OUTPUT_VOLUME >= 50)); then
            speaker_icon=$speaker_down_icon
        fi
        if (( OUTPUT_VOLUME <= 50)); then
            speaker_icon=$speaker_low_icon
        fi
        printf "%s %.0f%%" "$speaker_icon" "$OUTPUT_VOLUME"
    fi
}

Wifi() {
    IF_CHUNK=$(ifconfig | grep $wifi_ifname -6)
    IF_ACTIVE=$(echo "$IF_CHUNK" | grep "status" | cut -d ":" -f 2)
    printf "%s" "$wifiicon"
}

# Define the clock
Clock() {
    DATETIME=$(date "+%a %b %d, %I:%M:%S %p")
    printf "%s" "$DATETIME"
}

# Define the Battery
Battery () {
    BAT=$(apm -l)
    batteryicon=$battery_empty_icon
    if (( BAT > 75 ));
    then
        batteryicon=$battery_full_icon
    fi
    if (( BAT <= 75 && BAT > 50 ));
    then
        batteryicon=$battery_3qfull_icon
    fi
    if (( BAT <= 50 && BAT > 25 ));
    then
        batteryicon=$battery_halffull_icon
    fi
    if (( BAT <= 25 && BAT > 5 ));
    then
        batteryicon=$battery_qfull_icon
    fi
    if (( BAT <= 5 && BAT >= 0 ));
    then
        batteryicon=$battery_empty_icon
    fi
    echo -e "$batteryicon $BAT%"
}

# Print the clock

while true; do
    echo -e "%{l} $(Workspaces) %{r} $(Clock) $(Audio) $(Battery)  "
    sleep 0.2
done
