;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Mel Gray"
      user-mail-address (rot13 "zrytenl@tznvy.pbz"))

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
;;(setq doom-theme 'doom-one)
(setq doom-theme 'doom-city-lights)

(after! doom-themes
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t))

;; looks. vanity.
(set-frame-parameter (selected-frame) 'alpha '(97 . 50))
(add-to-list 'default-frame-alist '(alpha . (97 . 50)))
(custom-set-faces! '(doom-dashboard-banner :foreground "red" :weight bold))

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/Documents/org/"
      org-roam-directory "~/Documents/org/roam"
      org-hide-emphasis-markers t
      org-startup-indented t
      org-pretty-entities t
      org-startup-with-inline-images t
      org-src-tab-acts-natively t
      org-image-actual-width '(300)
      org-bullets-face-name (quote org-bullet-face))

(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))

;; org GTD setup
(setq org-agenda-files '("~/Documents/org/gtd/inbox.org"
                         "~/Documents/org/gtd/gtd.org"
                         "~/Documents/org/gtd/tickler.org"))

(setq org-capture-templates '(("t" "Todo [inbox]" entry
                               (file+headline "~/Documents/org/gtd/inbox.org" "Tasks")
                               "* TODO %i%?")
                              ("T" "Tickler" entry
                               (file+headline "~/Documents/org/gtd/tickler.org" "Tickler")
                               "* %i%? \n %U")))

(setq org-refile-targets '(("~/Documents/org/gtd/gtd.org" :maxlevel . 3)
                           ("~/Documents/org/gtd/someday.org" :level . 1)
                           ("~/Documents/org/gtd/done.org" :level . 4)
                           ("~/Documents/org/gtd/tickler.org" :maxlevel . 2)))

;; org projects
(setq org-publish-project-alist
      `(("route5.us"
         :base-directory "~/code/route5.us"
         :recursive t
         :publishing-directory "~/code/route5.us/build"
         :publishing-function org-html-publish-to-html)))

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

;; projectile config
(setq projectile-sort-order 'modification-time)
(setq projectile-project-search-path '("~/code"))

;; email config
(setq mu4e-maildir "~/Maildir/gmail")

;;(defun mu4e-headers-mark-all-unread-read ()
;;   "Put a ! \(read) mark on all visible unread messages"
;;(interactive)
;;(mu4e-headers-mark-for-each-if
;; (cons 'read nil)
;; (lambda (msg param)
;;   (memq 'unread (mu4e-msg-field msg :flags)))

;;(defun mu4e-headers-flag-all-read ()
;;  "Flag all visible messages as \"read\""
;;  (interactive)
;;  (mu4e-headers-mark-all-unread-read)
;;  (mu4e-mark-execute-all t))

;; dired
(after! dired
  (setq dired-listing-switches "-aBhl"))

;; zig
(use-package! zig-mode
  :hook ((zig-mode . lsp-deferred))
  :custom (zig-format-on-save t)
  :config
  (setq! zig-zig-bin "~/workspace/zig/build/zig")
  (after! lsp-mode
    (add-to-list 'lsp-language-id-configuration '(zig-mode . "zig"))
    (lsp-register-client
      (make-lsp-client
        :new-connection (lsp-stdio-connection "~/workspace/zls/zig-out/bin/zls")
        :major-modes '(zig-mode)
        :server-id 'zls))))

;; cc language config
(setq lsp-clangd-binary-path "/usr/local/bin/clangd")
(setq lsp-clients-clangd-args '("-j=3"
                                "--background-index"
                                "--clang-tidy"
                                "--completion-style=detailed"
                                "--header-insertion=never"
                                "--header-insertion-decorators=0"))

(add-hook 'cc-mode #'lsp)

;; evil-easymotion config "SPC-j"
(evilem-default-keybindings "SPC")

;; prettier.el - prettier javascript
(add-hook 'web-mode-hook #'global-prettier-mode)

;; rust
(setq rustic-display-spinner 'moon)
