alias ls="colorls -G"
alias ll="ls -la"
alias ..="cd .."
alias gst="git status"
alias pbcopy="xclip -sel c"
alias pbpaste="xclip -o -sel c"
alias lbar="lemonbar-xft -f 'Roboto Medium' -f 'Font Awesome 5 Free' -f 'Font Awesome 5 Brands' -f 'Font Awesome 5 Free Solid'"
alias vanity_formats='ffplay -f v4l2 -list_formats all -i /dev/video0'
alias vanity='ffplay -f v4l2 -input_format mjpeg -video_size 848x480 -i /dev/video0'

alias zource="source ~/.zshrc"

export TERM=wsvt25
export EDITOR="emacsclient -t"
export VISUAL="emacsclient -c -a emacs"
export PATH=$PATH:$HOME/.cargo/bin:$HOME/.local/bin:$HOME/.emacs.d/bin:$HOME/.local/bin

export GPG_TTY="$(tty)"
export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
gpgconf --launch gpg-agent

(cat ~/.cache/wal/sequences &)

PROMPT='[%n@%m: %1~] '
